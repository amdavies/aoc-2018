import sys
import time
import solvers
from solvers import *
from typing import Dict


def main(day):
    solver: Dict[str, any] = {
        "1": solvers.Day1,
        "2": solvers.Day2,
        "3": solvers.Day3,
        "4": solvers.Day4,
        "5": solvers.Day5,
        "6": solvers.Day6,
        "7": solvers.Day7,
        "8": solvers.Day8,
        "9": solvers.Day9,
        "10": solvers.Day10,
        "11": solvers.Day11,
        "12": solvers.Day12,
        "13": solvers.Day13,
        "14": solvers.Day14,
    }

    if day not in solver:
        print("No solutions exists for day (%s)" % day)
        exit(2)

    print("================")
    print("   Day " + day)
    print("================")

    inputs = open('./Inputs/day' + day).read().splitlines()

    s = time.time()
    print("\nPart 1 Solution: " + str(solver[day].part1(inputs)))
    e = time.time()
    print("Took: " + str(e - s))

    s = time.time()
    print("\nPart 2 Solution: " + str(solver[day].part2(inputs)))
    e = time.time()
    print("Took: " + str(e - s))

    print("")


if __name__ == "__main__":
    if len(sys.argv) == 1:
        day: str = input("Which day are we running? ")
    else:
        day: str = sys.argv[1]

    main(day)
