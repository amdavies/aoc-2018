from typing import NamedTuple


class Pt(NamedTuple('Pt', [('x', int), ('y', int)])):
    def __add__(self, other):
        return type(self)(self.x + other.x, self.y + other.y)

    @property
    def cross(self):
        return [self + d for d in [Pt(0, 1), Pt(1, 0), Pt(0, -1), Pt(-1, 0)]]

    @property
    def crisscross(self):
        return [self + d for d in [
            Pt(0, 1),
            Pt(1, 0),
            Pt(0, -1),
            Pt(-1, 0),
            Pt(-1, -1),
            Pt(-1, 1),
            Pt(1, 1),
            Pt(1, -1),
        ]]
