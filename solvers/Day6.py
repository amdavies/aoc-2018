from collections import Counter, defaultdict
import re


def part1(inputs):
    """Polymer explosions

    >>> part1(["1, 1","1, 6","8, 3","3, 4","5, 5","8, 9"])
    17
    >>> part1(open('./Inputs/day6').read().splitlines())
    3358
    """
    starting = []
    maxX = 0
    maxY = 0
    for input in inputs:
        (x, y) = input.split(', ')
        maxX = max(maxX, int(x))
        maxY = max(maxY, int(y))
        starting.append((int(x), int(y)))

    infinite = set()
    closest = {}
    for x in range(0, maxX + 1):
        for y in range(0, maxY + 1):
            minDist = 1e7
            closestPoint = None
            for pX, pY in starting:
                # Shortcut some calculations
                if pX > x + minDist or pY > y + minDist or \
                        pX < x - minDist or pY < y - minDist:
                    continue
                dist = abs(pX - x) + abs(pY - y)
                if dist < minDist:
                    minDist = dist
                    closestPoint = (pX, pY)
                elif dist == minDist:
                    closestPoint = None
            if x in (0, maxX) or y in (0, maxY):
                infinite.add(closestPoint)
            closest[(x, y)] = closestPoint

    for i in Counter(closest.values()).most_common():
        if i[0] not in infinite:
            return i[1]


def part2(inputs):
    """Carefully exploded polymers

    >>> part2(["1, 1","1, 6","8, 3","3, 4","5, 5","8, 9"])
    16
    >>> part2(open('./Inputs/day6').read().splitlines())
    45909
    """
    maxDist = 10000 if len(inputs) > 20 else 32
    starting = []
    maxX = 0
    maxY = 0
    for input in inputs:
        (x, y) = input.split(', ')
        maxX = max(maxX, int(x))
        maxY = max(maxY, int(y))
        starting.append((int(x), int(y)))

    inRegion = set()
    for x in range(0, maxX + 1):
        for y in range(0, maxY + 1):
            dSum = 0
            for pX, pY in starting:
                dSum += abs(pX - x) + abs(pY - y)
            if dSum < maxDist:
                inRegion.add((x, y))

    return len(inRegion)
