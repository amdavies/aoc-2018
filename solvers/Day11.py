from collections import defaultdict


def part1(inputs):
    """The Stars Align

    >>> part1(["42"])
    (21, 61)

    >>> part1(["5153"])
    (235, 18)
    """
    serial = int(inputs[0])
    cellVal = {}
    for x in range(1, 301):
        for y in range(1, 301):
            rackId = x + 10
            power = ((rackId * y + serial) * rackId)
            cellVal[(x, y)] = ((power // 100) % 10) - 5

    gridSum = {}
    best = None
    for x in range(1, 299):
        for y in range(1, 299):
            sum = cellVal[(x, y)] + cellVal[(x + 1, y)] + cellVal[(x + 2, y)] +\
                  cellVal[(x, y + 1)] + cellVal[(x + 1, y + 1)] +\
                  cellVal[(x + 2, y + 1)] + cellVal[(x, y + 2)] +\
                  cellVal[(x + 1, y + 2)] + cellVal[(x + 2, y + 2)]
            gridSum[(x, y)] = sum
            if best is None or sum > gridSum[best]:
                best = (x, y)

    return best


def part2(inputs):
    """Marble Mania x 100

    >>> part2(["18"])
    (90, 269, 16)

    >>> part2(["42"])
    (232, 251, 12)

    >>> part2(["5153"])
    (236, 227, 12)
    """
    serial = int(inputs[0])
    cellVal = {}
    # Calculate val for each cell
    for x in range(1, 301):
        for y in range(1, 301):
            rackId = x + 10
            power = ((rackId * y + serial) * rackId)
            cellVal[(x, y)] = ((power // 100) % 10) - 5

    # Calculate a cumulative value for each cell + all previous in a grid to 0, 0
    cSum = defaultdict(int)
    for x in range(1, 301):
        for y in range(1, 301):
            cSum[(x, y)] = cellVal[(x, y)] + cSum[(x - 1, y)] + \
                           cSum[(x, y - 1)] - cSum[(x - 1, y - 1)]

    # Using cumulative, calculate multiple grid sizes
    best = None
    gSum = {}
    for x in range(1, 301):
        for y in range(1, 301):
            for size in range(1, 301 - max(x, y)):
                sum = cSum[(x + size, y + size)] + cSum[(x, y)] - \
                      cSum[(x + size, y)] - cSum[(x, y + size)]
                gSum[(x, y, size)] = sum
                if best is None or sum > gSum[best]:
                    best = (x, y, size)

    return best[0] + 1, best[1] + 1, best[2]
