import re


def part1(inputs):
    """The Stars Align

    >>> part1(open('./Inputs/day10-ex').read().splitlines())
    #___#__###
    #___#___#_
    #___#___#_
    #####___#_
    #___#___#_
    #___#___#_
    #___#___#_
    #___#__###

    >>> part1(open('./Inputs/day10').read().splitlines())
    #####___#____#_____###__#____#_____###____##____######__#____#
    #____#__#____#______#___#____#______#____#__#___#_______#____#
    #____#___#__#_______#____#__#_______#___#____#__#________#__#_
    #____#___#__#_______#____#__#_______#___#____#__#________#__#_
    #####_____##________#_____##________#___#____#__#####_____##__
    #____#____##________#_____##________#___######__#_________##__
    #____#___#__#_______#____#__#_______#___#____#__#________#__#_
    #____#___#__#___#___#____#__#___#___#___#____#__#________#__#_
    #____#__#____#__#___#___#____#__#___#___#____#__#_______#____#
    #####___#____#___###____#____#___###____#____#__######__#____#
    """
    lines = [[int(i) for i in re.findall(r'-?\d+', l)] for l in inputs]

    seconds = 3
    smallest = None
    for i in range(20000):
        minx = min(x + i * vx for (x, y, vx, vy) in lines)
        if minx < 0:
            continue
        miny = min(y + i * vy for (x, y, vx, vy) in lines)
        if miny < 0:
            continue
        maxx = max(x + i * vx for (x, y, vx, vy) in lines)
        maxy = max(y + i * vy for (x, y, vx, vy) in lines)

        boxSize = maxx - minx + maxy - miny
        if smallest and boxSize > smallest:
            break
        bxx, bxn, byx, byn = maxx, minx, maxy, miny
        smallest = boxSize
        seconds = i

    map = [['_'] * (bxx - bxn + 1) for j in range(byx - byn + 1)]
    for (x, y, vx, vy) in lines:
        map[y + seconds * vy - byn][x + seconds * vx - bxn] = '#'

    for m in map:
        print(''.join(m))


def part2(inputs):
    """Marble Mania x 100

    >>> part2(open('./Inputs/day10-ex').read().splitlines())
    3
    >>> part2(open('./Inputs/day10').read().splitlines())
    10605
    """
    lines = [[int(i) for i in re.findall(r'-?\d+', l)] for l in inputs]

    seconds = 3
    smallest = None
    for i in range(20000):
        minx = min(x + i * vx for (x, y, vx, vy) in lines)
        if minx < 0:
            continue
        maxx = max(x + i * vx for (x, y, vx, vy) in lines)
        miny = min(y + i * vy for (x, y, vx, vy) in lines)
        maxy = max(y + i * vy for (x, y, vx, vy) in lines)

        boxSize = maxx - minx + maxy - miny
        if not smallest or boxSize < smallest:
            smallest = boxSize
            seconds = i
        if boxSize > smallest:
            break

    return seconds
