import re
from collections import deque


def getScore(players, finalMarble):
    out = deque([0])
    scores = {k: 0 for k in range(players)}
    for inplay in range(1, finalMarble + 1):
        if inplay % 23 == 0:
            out.rotate(7)
            score = out.pop()
            scores[inplay % players] += inplay + score
            out.rotate(-1)
        else:
            out.rotate(-1)
            out.append(inplay)

    return max(scores.values())


def part1(inputs):
    """Marble Mania

    >>> part1(["9 players; last marble is worth 25 points"])
    32
    >>> part1(["10 players; last marble is worth 1618 points"])
    8317
    >>> part1(["13 players; last marble is worth 7999 points"])
    146373
    >>> part1(["17 players; last marble is worth 1104 points"])
    2764
    >>> part1(["21 players; last marble is worth 6111 points"])
    54718
    >>> part1(["30 players; last marble is worth 5807 points"])
    37305
    >>> part1(open('./Inputs/day9').read().splitlines())
    374690
    """
    players, finalMarble = (int(x) for x in re.findall(r'-?\d+', inputs[0]))

    return getScore(players, finalMarble)


def part2(inputs):
    """Marble Mania x 100

    >>> part2(open('./Inputs/day9').read().splitlines())
    3009951158
    >>> part2(["413 players; last marble is worth 71082 points"])
    3498287922
    """
    players, finalMarble = (int(x) for x in re.findall(r'-?\d+', inputs[0]))

    return getScore(players, finalMarble * 100)
