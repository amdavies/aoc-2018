def part1(inputs):
    """Polymer explosions

    >>> part1(["9"])
    '5158916779'
    >>> part1(["5"])
    '0124515891'
    >>> part1(["18"])
    '9251071085'
    >>> part1(["2018"])
    '5941429882'
    >>> part1(["327901"])
    '1115317115'
    """
    count = int(inputs[0])
    recipes = [3, 7]
    e1, e2 = 0, 1
    for it in range(count + 10):
        sum = recipes[e1] + recipes[e2]
        recipes.extend(divmod(sum, 10) if sum >= 10 else [sum])
        e1 = (e1 + 1 + int(recipes[e1])) % len(recipes)
        e2 = (e2 + 1 + int(recipes[e2])) % len(recipes)

    return ''.join([str(digit) for digit in recipes[count:count+10]])


def part2(inputs):
    """Carefully exploded polymers

    >>> part2(["51589"])
    9

    >>> part2(["0124"])
    5

    >>> part2(["92510"])
    18

    >>> part2(["59414"])
    2018

    >> part2(["327901"]) # This takes ~1 minute to run
    20229822
    """
    value = inputs[0]
    digits = [int(digit) for digit in str(value)]
    recipes = [3, 7]
    e1, e2 = 0, 1

    count = 0
    while recipes[-len(digits):] != digits and recipes[-len(digits) - 1:-1] != digits and count < 20000000:
        count += 1
        sum = recipes[e1] + recipes[e2]
        recipes.extend(divmod(sum, 10) if sum >= 10 else [sum])

        e1 = (e1 + 1 + int(recipes[e1])) % len(recipes)
        e2 = (e2 + 1 + int(recipes[e2])) % len(recipes)

    return (len(recipes) - len(digits) - (0 if recipes[-len(digits):] == digits else 1))
