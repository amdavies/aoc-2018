import string

up = string.ascii_uppercase
low = string.ascii_lowercase
ll = dict(zip(low + up, up + low))


def react(s):
    """React adjacent matching opposite polymers "recursively"

    >>> react("aAb")
    ['b']
    >>> react("aBbAc")
    ['c']
    """
    out = list()
    for c in s:
        if len(out) == 0:
            out = [c]
        elif ll[c] == out[-1]:
            out.pop()
        else:
            out.append(c)
    return out


def part1(inputs):
    """Polymer explosions

    >>> part1(["dabAcCaCBAcCcaDA"])
    10
    >>> part1(open('./Inputs/day5').read().splitlines())
    11668
    """
    return len(react(inputs[0]))


def part2(inputs):
    """Carefully exploded polymers

    >>> part2(["dabAcCaCBAcCcaDA"])
    4
    >>> part2(open('./Inputs/day5').read().splitlines())
    4652
    """
    input = react(inputs[0])
    best = len(input)
    for c in low:
        s = [x for x in input if x != c and x != ll[c]]
        best = min(best, len(react(s)))

    return best
