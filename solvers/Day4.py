from collections import defaultdict


def part1(inputs):
    """Sleepiest guards most sleepy minute

    >>> part1(open('./Inputs/day4-ex').read().splitlines())
    240
    >>> part1(open('./Inputs/day4').read().splitlines())
    140932
    """
    inputs.sort()
    times = defaultdict(int)
    gTimes = defaultdict(int)
    for entry in inputs:
        (dt, message) = entry.split('] ')
        minute = int(dt[-2:])
        if "Guard" in message:
            no = int(message.split()[1][1:])
        elif message == "falls asleep":
            start = minute
        else:
            gTimes[no] += minute - start
            for m in range(start, minute):
                times[(no, m)] += 1

    (guard, _) = max(gTimes.items(), key=lambda i: i[1])

    match = None
    for time, count in times.items():
        if time[0] != guard:
            continue
        if match is None or count > times[match]:
            match = time

    return match[0] * match[1]


def part2(inputs):
    """Sleepiest minute across all guard

    >>> part2(open('./Inputs/day4-ex').read().splitlines())
    4455
    >>> part2(open('./Inputs/day4').read().splitlines())
    51232
    """
    inputs.sort()
    times = defaultdict(int)
    for entry in inputs:
        (dt, message) = entry.split('] ')
        minute = int(dt[-2:])
        if 'Guard' in message:
            no = int(message.split()[1][1:])
        elif message == 'falls asleep':
            start = minute
        else:
            for m in range(start, minute):
                times[(no, m)] += 1

    match = None
    for time, count in times.items():
        if match is None or count > times[match]:
            match = time

    return match[0] * match[1]
