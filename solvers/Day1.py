from typing import Dict, List


def part1(inputs: List[str]):
    """Return the sum of all elements in the input

    >>> part1(["+1", "+1", "+1"])
    3
    >>> part1(["+1", "+1", "-2"])
    0
    >>> part1(["-1", "-2", "-3"])
    -6
    >>> part1(open('./Inputs/day1').read().splitlines())
    536
    """
    return sum(int(x.strip()) for x in inputs)


def part2(inputs: List[str]):
    """Return the first repeated frequency calculated while summing inputs

    >>> part2(["+1", "-1"])
    0
    >>> part2(["+3", "+3", "+4", "-2", "-4"])
    10
    >>> part2(["-6", "+3", "+8", "+5", "-6"])
    5
    >>> part2(["+7", "+7", "-2", "-7", "-4"])
    14
    >>> part2(open('./Inputs/day1').read().splitlines())
    75108
    """
    result: int = 0
    found: Dict[int, bool] = {0: True}
    for i in range(0, 200):
        for delta in inputs:
            if delta == '':
                continue

            result += int(delta)
            if result in found:
                return result

            found[result] = True
    raise Exception("Took 200 iterations, no solution found")
