import re
from collections import defaultdict


def part1(inputs):
    """Find the amount of squares that are covered by multiple claims

    >>> part1(["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"])
    4
    >>> part1(open('./Inputs/day3').read().splitlines())
    104712
    """
    claims = map(lambda s: map(int, re.findall(r'-?\d+', s)), inputs)
    fabric = defaultdict(int)
    for (_, left, top, width, height) in claims:
        for xpos in range(left, left + width):
            for ypos in range(top, top + height):
                fabric[(xpos, ypos)] += 1

    return len([k for k in fabric if fabric[k] > 1])


def part2(inputs):
    """Find a claim that has no overlaps

    >>> part2(["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"])
    3
    >>> part2(open('./Inputs/day3').read().splitlines())
    840
    """
    validClaims = {}
    claims = map(lambda s: map(int, re.findall(r'-?\d+', s)), inputs)
    fabric = {}
    for (id, left, top, width, height) in claims:
        validClaims[id] = True
        for xpos in range(left, left + width):
            for ypos in range(top, top + height):
                if (xpos, ypos) not in fabric:
                    fabric[(xpos, ypos)] = {"claim": id, "count": 0}
                else:
                    cId = fabric[(xpos, ypos)]['claim']
                    if cId in validClaims:
                        del validClaims[cId]
                    if id in validClaims:
                        del validClaims[id]

                fabric[(xpos, ypos)]['count'] += 1

    for claim in validClaims:
        return claim
