def parseNode(vals):
    cCount, mCount = vals[:2]
    vals = vals[2:]
    nodeVal = []

    total = 0
    for _ in range(cCount):
        tot, vals, value = parseNode(vals)
        total += tot
        nodeVal.append(value)

    if cCount == 0:
        val = sum(vals[:mCount])
    else:
        val = sum(
            nodeVal[k - 1]
            for k in vals[:mCount]
            if 0 < k <= len(nodeVal)
        )

    total += sum(vals[:mCount])
    return total, vals[mCount:], val


def part1(inputs):
    """Polymer explosions

    >>> part1(["2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"])
    138
    >>> part1(open('./Inputs/day8').read().splitlines())
    45865
    """
    inputs = [int(x) for x in inputs[0].split()]
    total, _, _ = parseNode(inputs)

    return total


def part2(inputs):
    """Carefully exploded polymers

    >>> part2(["2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2"])
    66
    >>> part2(open('./Inputs/day8').read().splitlines())
    22608
    """
    inputs = [int(x) for x in inputs[0].split()]
    _, _, value = parseNode(inputs)

    return value
