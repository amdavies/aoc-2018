import itertools
from collections import Counter
from typing import List


def part1(inputs: List[str]):
    """Calculate as checksum by multiplying number of IDs with 3 duplicate
    letters and number of IDs with 2 duplicate letters

    Input is an array of box IDs

    >>> part1(["abcdef","bababc","abbcde","abcccd","aabcdd","abcdee","ababab"])
    12
    >>> part1(open('./Inputs/day2').read().splitlines())
    5390
    """
    out = [0, 0]
    for id in inputs:
        counts = [j for _, j in Counter(id).most_common()]
        if 3 in counts:
            out[0] += 1
        if 2 in counts:
            out[1] += 1
    return out[0] * out[1]


def part2(inputs: List[str]):
    """Find common letters between boxes that only have 1 character difference

    Input is an array of box IDs

    >>> part2(["abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"])
    'fgij'
    >>> part2(open('./Inputs/day2').read().splitlines())
    'nvosmkcdtdbfhyxsphzgraljq'
    """
    for id, alt in itertools.combinations(inputs, 2):
        diffs = 0
        remainder = ''
        for a, b in zip(id, alt):
            if a == b:
                remainder += a
                continue
            diffs += 1
            if diffs > 1:
                break
        if diffs == 1:
            return remainder
