from collections import defaultdict
import re
import string


def part1(inputs):
    """Polymer explosions

    >>> part1(open('./Inputs/day7').read().splitlines())
    'GJKLDFNPTMQXIYHUVREOZSAWCB'
    >>> part1(open('./Inputs/day7-mm').read().splitlines())
    'GRTAHKLQVYWXMUBCZPIJFEDNSO'
    >>> part1(open('./Inputs/day7-rs').read().splitlines())
    'IOFSJQDUWAPXELNVYZMHTBCRGK'
    """
    blocks = defaultdict(int)
    blocking = defaultdict(set)
    pattern = re.compile("S.*(?P<before>[A-Z]).*(?P<after>[A-Z]).*")
    for requirement in inputs:
        steps = re.match(pattern, requirement)
        before, after = steps.groups()
        blocks[after] += 1
        blocking[before].add(after)

    out = ''
    remaining = list(string.ascii_uppercase)
    while remaining:
        char = [x for x in remaining if blocks[x] == 0][0]
        out += char
        remaining.remove(char)
        for blocked in blocking[char]:
            blocks[blocked] -= 1

    return out


def part2(inputs):
    """Carefully exploded polymers

    >>> part2(open('./Inputs/day7').read().splitlines())
    967
    >>> part2(open('./Inputs/day7-mm').read().splitlines())
    1115
    >>> part2(open('./Inputs/day7-rs').read().splitlines())
    931
    """
    blocks = defaultdict(int)
    blocking = defaultdict(set)
    pattern = re.compile("S.*(?P<before>[A-Z]).*(?P<after>[A-Z]).*")
    for requirement in inputs:
        steps = re.match(pattern, requirement)
        before, after = steps.groups()
        blocks[after] += 1
        blocking[before].add(after)

    remaining = list(string.ascii_uppercase)
    time = 0
    workers = {k: None for k in range(5)}
    while remaining or any(workers.values()):
        for worker in [k for k in workers if workers[k]]:
            letter, step = workers[worker]
            if step == ord(letter) - ord('A') + 61:
                for blocked in blocking[letter]:
                    blocks[blocked] -= 1
                workers[worker] = None
            else:
                workers[worker] = (letter, step + 1)
        for worker in [k for k in workers if not workers[k]]:
            available = [x for x in remaining if blocks[x] == 0]
            if available:
                char = available[0]
                remaining.remove(char)
                workers[worker] = (char, 1)
        time += 1

    return time - 1
