import re
from advent import asm


def part1(inputs):
    """Polymer explosions

    >>> part1(["Before: [3, 2, 1, 1]","9 2 1 2","After:  [3, 2, 2, 1]"])
    1

    >>> part1(open('./Inputs/day16').read().splitlines())
    677
    """
    cpu = asm.ASM()

    before = list()
    instruction = list()
    count = 0
    for line in inputs:
        if 'Before' in line:
            before = list(map(int, re.findall(r'\d+', line)))
        elif 'After' in line:
            after = list(map(int, re.findall(r'\d+', line)))
            opCount = 0
            for method in cpu.methods:
                cpu.restoreState(before)
                cpu.execute(instruction, method)
                if cpu.register == after:
                    opCount += 1
            if opCount >= 3:
                count += 1

        else:
            instruction = list(map(int, re.findall(r'\d+', line)))

    return count


def part2(inputs):
    """Carefully exploded polymers

    >>> part2(open('./Inputs/day16').read().splitlines())
    540

    >>> part2(open('./Inputs/day16-rs').read().splitlines())
    610
    """
    cpu = asm.ASM()

    known = set()
    before = list()
    instruction = list()
    count = 0
    candidates = {}
    lastline = 2
    for i in range(16):
        candidates[i] = set(cpu.methods) - set(known)
    for line in inputs:
        if 'Before' in line:
            before = list(map(int, re.findall(r'\d+', line)))
        elif 'After' in line:
            lastline += 4
            after = list(map(int, re.findall(r'\d+', line)))
            opCount = 0
            for method in cpu.methods:
                cpu.restoreState(before)
                code = instruction[0]
                cpu.execute(instruction, method)
                if cpu.register != after:
                    candidates[code].discard(method)
            if opCount >= 3:
                count += 1
        else:
            instruction = list(map(int, re.findall(r'\d+', line)))
    while len(known) < len(cpu.methods):
        for i in range(16):
            if len(candidates[i]) == 1:
                method = candidates[i].pop()
                cpu.opcodes[i] = method
                known.add(method)
        for i in range(16):
            if len(candidates[i]) > 1:
                candidates[i] -= known

    cpu.restoreState([0, 0, 0, 0])
    for line in inputs[lastline:]:
        instruction = list(map(int, re.findall(r'\d+', line)))
        if len(instruction) != 4:
            continue
        cpu.execute(instruction)

    return cpu.register[0]
