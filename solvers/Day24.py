import math
import re


class Group:
    nextid = 1
    def __init__(self, units, health, damage, attack, initiative, immune, weak, side):
        self.id = Group.nextid
        Group.nextid += 1
        self.units = units
        self.attack = attack
        self.health = health
        self.damage = damage
        self.attack = attack
        self.initiative = initiative
        self.immune = immune
        self.weak = weak
        self.side = side

    def power(self):
        return self.units * self.damage

    def damageTo(self, group):
        if self.attack in group.immune:
            return 0
        elif self.attack in group.weak:
            return self.power() * 2
        else:
            return self.power()



def part1(inputs):
    """
    >>> part1(open('Inputs/day24-ex').read().splitlines())
    5216


    >>> part1(open('Inputs/day24').read().splitlines())
    14799
    """
    groups = []

    pattern = re.compile(r"(\d+) units each with (\d+) hit points (\([^)]*\) )?with an attack that does (\d+) (\w+) damage at initiative (\d+)")

    side = 0
    for line in inputs:
        if not line:
            continue
        if 'Immune System:' in line:
            side = 0
            continue
        if 'Infection:' in line:
            side = 1
            continue

        s = pattern.match(line)
        units, hp, extra, damage, dtype, init = s.groups()
        immune = []
        weak = []
        if extra:
            extra = extra.rstrip(" )").lstrip("(")
            for s in extra.split("; "):
                if s.startswith("weak to "):
                    weak = s[8:].split(", ")
                elif s.startswith("immune to "):
                    immune = s[10:].split(", ")
                else:
                    assert False
        group = Group(int(units), int(hp), int(damage), dtype, int(init), set(immune), set(weak), side)
        groups.append(group)

    return fight(groups)[0]


def part2(inputs):
    """
    >>> part2(open('Inputs/day24-ex').read().splitlines())
    51

    >>> part2(open('Inputs/day24').read().splitlines())
    4428
    """
    groups = []

    pattern = re.compile(
        r"(\d+) units each with (\d+) hit points (\([^)]*\) )?with an attack that does (\d+) (\w+) damage at initiative (\d+)")

    side = 0
    for line in inputs:
        if not line:
            continue
        if 'Immune System:' in line:
            side = 0
            continue
        if 'Infection:' in line:
            side = 1
            continue

        s = pattern.match(line)
        units, hp, extra, damage, dtype, init = s.groups()
        immune = []
        weak = []
        if extra:
            extra = extra.rstrip(" )").lstrip("(")
            for s in extra.split("; "):
                if s.startswith("weak to "):
                    weak = s[8:].split(", ")
                elif s.startswith("immune to "):
                    immune = s[10:].split(", ")
                else:
                    assert False
        group = Group(int(units), int(hp), int(damage), dtype, int(init), set(immune), set(weak), side)
        groups.append(group)

    boost = 1
    while True:
        score, winner = fight(groups, boost)
        if winner == 0:
            return score
        boost += 1


def fight(originalGroups, boost=0):
    groups = []
    # Boost the immune system
    for group in originalGroups:
        newDamage = group.damage + (boost if group.side == 0 else 0)
        groups.append(Group(
            group.units,
            group.health,
            newDamage,
            group.attack,
            group.initiative,
            group.immune,
            group.weak,
            group.side
        ))
    while True:
        # Target selection
        groups = sorted(groups, key=lambda g: (-g.power(), -g.initiative))
        chosen = set()
        gt = {}
        for group in groups:
            targets = sorted([target for target in groups if
                              target.side != group.side and target.id not in chosen and group.damageTo(target) > 0],
                             key=lambda target: (-group.damageTo(target), -target.power(), -target.initiative))
            if targets:
                gt[group.id] = targets[0]
                chosen.add(targets[0].id)

        # Attack phase
        groups = sorted(groups, key=lambda g: -g.initiative)
        haveKilled = False
        for group in groups:
            if group.id not in gt:
                continue
            target = gt[group.id]
            damage = group.damageTo(target)
            killed = min(target.units, math.floor(damage / target.health))
            target.units -= killed
            if killed:
                haveKilled = True

        if not haveKilled:
            return 0, 1

        groups = [g for g in groups if g.units > 0]

        u0 = sum([g.units for g in groups if g.side == 0])
        u1 = sum([g.units for g in groups if g.side == 1])
        if not u0:
            return u1, 1
        elif not u1:
            return u0, 0
