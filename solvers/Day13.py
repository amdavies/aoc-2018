from collections import defaultdict
from typing import List, Tuple, Dict


class Cart:
    def __init__(self, pos: Tuple[int, int], direction: int):
        self.position = pos
        self.direction = direction
        self.cross_mod = 0
        self.dead = False


def move(cart, tracks):
    x, y = cart.position
    direction = cart.direction
    track = tracks[y][x]
    if track == '+':
        directions = '^<v>'
        turn = [1, 0, -1][cart.cross_mod]
        direction = directions[(turn + directions.index(cart.direction)) % len(directions)]
        cart.cross_mod = (cart.cross_mod + 1) % 3
    dX, dY, direction = {
        '|': {
            '^': (0, -1, direction),
            'v': (0, 1, direction)
        },
        '-': {
            '>': (1, 0, direction),
            '<': (-1, 0, direction)
        },
        '/': {
            '^': (1, 0, '>'),
            '>': (0, -1, '^'),
            'v': (-1, 0, '<'),
            '<': (0, 1, 'v')
        },
        '\\': {
            '^': (-1, 0, '<'),
            '<': (0, -1, '^'),
            'v': (1, 0, '>'),
            '>': (0, 1, 'v')
        },
        '+': {
            '^': (0, -1, direction),
            '>': (1, 0, direction),
            'v': (0, 1, direction),
            '<': (-1, 0, direction)
        }
    }[track][direction]
    cart.direction = direction
    cart.position = (x + dX, y + dY)
    return cart


def part1(inputs):
    """Mine Cart Madness

    >>> part1(open('./Inputs/day13-ex').read().splitlines())
    (7, 3)

    >>> part1(open('./Inputs/day13').read().splitlines())
    (53, 133)

    >> part1(open('./Inputs/day13-rs').read().splitlines())
    (7, 3)
    """
    directions = {
        '^': '|',
        'v': '|',
        '>': '-',
        '<': '-'
    }

    carts = set()
    track = []

    y = 0
    for l in inputs:
        track.append([])
        for x, d in enumerate(l):
            if d in directions:
                carts.add(Cart((x, y), d))
                track[y].append(directions[d])
            else:
                track[y].append(d)
        y += 1

    first_crash = None
    for tick in range(1530):
        new_carts = set()
        carts = sorted(carts, key=lambda t: (t.position[1], t.position[0]), reverse=True)
        while carts:
            cart = carts.pop()
            new_cart = move(cart, track)
            if new_cart.position not in [c.position for c in carts]\
                    and new_cart.position not in [c.position for c in new_carts]:
                new_carts.add(new_cart)
            else:
                return new_cart.position
                first_crash = first_crash or new_cart
                new_carts.discard(new_cart)
                if new_cart in carts:
                    carts.remove(new_cart)
                if new_cart in new_cart_directions:
                    del new_cart_directions[new_cart]
                if new_cart in new_cart_states:
                    del new_cart_states[new_cart]
                continue
            new_carts.add(new_cart)
        carts = new_carts
        if len(carts) <= 1:
            return first_crash, carts.pop()

    for cart in carts:
        track[cart.position[1]][cart.position[0]] = 'C'

    for line in track:
        print(''.join(line))

    return first_crash.position


def part2(inputs):
    """
    >>> part2(open('./Inputs/day13-ex2').read().splitlines())
    (6, 4)

    >>> part2(open('./Inputs/day13').read().splitlines())
    (111, 68)
    """
    directions = {
        '^': '|',
        'v': '|',
        '>': '-',
        '<': '-'
    }

    carts = set()
    track = []

    y = 0
    for l in inputs:
        track.append([])
        for x, d in enumerate(l):
            if d in directions:
                carts.add(Cart((x, y), d))
                track[y].append(directions[d])
            else:
                track[y].append(d)
        y += 1

    first_crash = None
    for tick in range(100000):
        new_carts = set()
        carts = sorted(carts, key=lambda t: (t.position[1], t.position[0]), reverse=True)
        while carts:
            cart = carts.pop()
            new_cart = move(cart, track)
            if new_cart.position not in [c.position for c in carts]\
                    and new_cart.position not in [c.position for c in new_carts]:
                new_carts.add(new_cart)
            else:
                first_crash = first_crash or new_cart
                collision = [c for c in carts if c.position == new_cart.position]
                if collision:
                    carts.remove(collision[0])
                collision = [c for c in new_carts if c.position == new_cart.position]
                if collision:
                    new_carts.discard(collision[0])
                continue
            new_carts.add(new_cart)
        carts = new_carts
        if len(carts) <= 1:
            return carts.pop().position
