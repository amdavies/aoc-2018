from collections import defaultdict


def toRule(pots, pot):
    out = ''
    out += '#' if pots[pot - 2] else '.'
    out += '#' if pots[pot - 1] else '.'
    out += '#' if pots[pot] else '.'
    out += '#' if pots[pot + 1] else '.'
    out += '#' if pots[pot + 2] else '.'
    return out


def doTheThing(pots, rules, generations):
    for generation in range(1, generations + 1):
        nextpots = defaultdict(int)
        start = min([k for k in pots]) - 2
        end = max([k for k in pots]) + 2
        for pot in range(start, end + 1):
            potRule = toRule(pots, pot)
            for rule in rules:
                if rule == potRule:
                    nextpots[pot] = 1
                    break
        pots = nextpots

    return pots


def part1(inputs):
    """The Stars Align

    >>> part1(open('./Inputs/day12-ex').read().splitlines())
    325
    >>> part1(open('./Inputs/day12').read().splitlines())
    2349
    """
    pots = defaultdict(int)

    for pot, state in enumerate(inputs[0][15:]):
        if state == '#':
            pots[pot] = 1

    rules = set()
    for rule in inputs[2:]:
        if rule[-1:] == '#':
            rules.add(rule[:5])

    pots = doTheThing(pots, rules, 20)

    return sum(pots)


def part2(inputs):
    """Marble Mania x 100

    >>> part2(open('./Inputs/day12').read().splitlines())
    2100000001168
    """
    pots = defaultdict(int)

    for pot, state in enumerate(inputs[0][15:]):
        if state == '#':
            pots[pot] = 1

    rules = set()
    for rule in inputs[2:]:
        if rule[-1:] == '#':
            rules.add(rule[:5])

    pots = doTheThing(pots, rules, 1000)

    return sum(pots) + (49999999000 * 42)
